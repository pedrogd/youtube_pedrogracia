package main;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Date;

public class Video {

	private Video video;
	private static String titulo;
	private static int numLike;
	public static Date FechaCreacion;
	public static ArrayList<Comentario> comentarios;

	public Video(String titulo) {
		this.titulo = titulo;
		this.numLike = 0;
		this.FechaCreacion = new Date();
		this.comentarios = new ArrayList<Comentario>();
		boolean menu = true;
		while (menu) {
			menu = showMenu();
		}

	}

	public static boolean showMenu() {
		mostrarMenu();
		Scanner scanner = new Scanner(System.in);
		int opcion = scanner.nextInt();
		switch (opcion) {
		case 0:
			System.out.println("ADIOS");
			return false;
		case 1:
			nuevoComentario();
			return true;
		case 2:
			like();
			return true;
		case 3:
			mostrarComentarios();
			return true;
		case 4:
			mostrarInfoVideo();
			return true;
		default:
			System.out.println("Opcion no valida. Intentelo de nuevo.");
			return true;
		}

	}

	private static void nuevoComentario() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Introduce comentario: ");
		String texto = scanner.nextLine();
		System.out.println("Usuario: ");
		String usuario = scanner.nextLine();
		Comentario comentario = new Comentario(texto);
		comentarios.add(comentario);
	}

	private static void like() {
		numLike++;
		System.out.println(numLike + "\\u001B[31m" + "ME GUSTA" + "\\u001B[0m");

	}

	private static void mostrarComentarios() {

		for (int i = 0; i < comentarios.size(); i++) {
			Comentario comentario = comentarios.get(i);
			System.out.println("Comentario: " + comentario.getTexto() + ", del usuario " + comentario.getNombre()
					+ ", con fecha " + comentario.getFechaCreacion());
		}

	}

	public static void mostrarMenu() {
		System.out.println("|---" + getNombre() + "---|" + "\n1- Nuevo comentario\n" + "2- Like\n"
				+ "3- Mostrar Comentarios\n" + "4-Mostrar estadísticas\n" + "0- Salir\n" + "|-------------------|");

	}

	private static void mostrarInfoVideo() {
		System.out.println("Video: " + getNombre() + " en fecha: " + getFechaCreacion() + " con " + getnumLike()
				+ " likes y " + comentarios.size() + " comentarios.");

	}

	public void setNombre(String titulo) {
		this.titulo = titulo;

	}

	public static String getNombre() {
		return titulo;
	}

	public void setFechaCreacion(Date FechaCreacion) {
		this.FechaCreacion = FechaCreacion;
	}

	public static Date getFechaCreacion() {
		return FechaCreacion;
	}

	public void setnumLike(int numLike) {
		this.numLike = numLike;
	}

	public static int getnumLike() {
		return numLike;
	}
}