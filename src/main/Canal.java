package main;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Date;

public class Canal {

	private static String nombre;
	private static Date fechaDeCreacion;
	private static Canal canal;
	public static ArrayList<Video> videos;

	public void Canal(String nombre) {
		this.nombre = nombre;
		setFechaDeCreacion(new Date());
		this.videos = new ArrayList<Video>();
		boolean menu = true;
		while (menu) {
			menu = showMenu();
		}
	}

	public boolean showMenu() {
		mostrarMenu();
		Scanner scanner = new Scanner(System.in);
		int opcion = scanner.nextInt();
		switch (opcion) {
		case 0:
			System.out.println("ADIOS");
			return false;
		case 1:
			System.out.println("gola");
			Canal canal = nuevoVideo();
			return true;
		case 2:
			Canal canal1 = seleccionarVideo();
			return true;
		case 3:
			Canal canal2 = mostrarEstadisticas();
			return true;
		case 4:
			Canal canal3 = mostrarInfoVideo();
			return true;
		default:
			System.out.println("Opcion no valida. Intentelo de nuevo.");
			return true;

		}

	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public static String getNombre() {
		return nombre;
	}

	public ArrayList<Video> getVideos() {
		return videos;

	}

	public void setFechaDeCreacion(Date fecha) {
		this.fechaDeCreacion = fecha;
	}

	public Date getFechaDeCreacion() {
		return fechaDeCreacion;
	}

	private static int mostrarMenu() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("|---" + getNombre() + "--|");
		System.out.println("1- Nuevo video");
		System.out.println("2- Seleccionar video");
		System.out.println("3- Mostrar estadísticas");
		System.out.println("4- Mostrar info videos");
		System.out.println("0- Salir");
		int option = scanner.nextInt();
		return option;
	}

	private static Canal nuevoVideo() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Indica el nombre del Video. ");
		String nombre = scanner.nextLine();
		Video video = new Video(nombre);
		videos.add(video);
		video.mostrarMenu();
		return null;
	}

	private static Canal seleccionarVideo() {
		Scanner scanner = new Scanner(System.in);
		if (videos.size() == 0) {
			nuevoVideo();
		} else {
			System.out.println("Selecciona el video");
			Video video;
			for (int i = 0; i < videos.size(); i++) {
				video = videos.get(i);
				System.out.println((i) + "-Video: " + video.getNombre() + " en fecha: " + video.getFechaCreacion()
						+ " con " + video.getnumLike() + " y " + video.comentarios.size() + " comentarios.");
			}
			int opcion = scanner.nextInt();
			video = videos.get(opcion);
			video.mostrarMenu();
		}
		return canal;

	}

	private Canal mostrarEstadisticas() {
		System.out.println("Nombre de canal: " + getNombre() + " en fecha " + getFechaDeCreacion() + " con "
				+ videos.size() + " videos.");
		return null;
	}

	private Canal mostrarInfoVideo() {
		Video video;
		for (int i = 0; i < videos.size(); i++) {
			video = videos.get(i);
			System.out.println("Video: " + video.getNombre() + " con fecha: " + video.getFechaCreacion() + " con "
					+ video.getnumLike() + " likes y " + video.comentarios.size() + " comentarios");
		}
		return null;
	}
}
