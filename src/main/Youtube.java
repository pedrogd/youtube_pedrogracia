package main;

import java.util.ArrayList;
import java.util.Scanner;
import model.Canal;
import model.Comentario;
import model.Video;

public class Youtube {
	static ArrayList<Canal> canales;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		canales = new ArrayList();
		boolean menu = true;
		while (menu) {
			menu = showMenu();
		}
	}

	public static boolean showMenu() {
		mostrarMenu();
		Scanner scanner = new Scanner(System.in);
		int opcion = scanner.nextInt();
		switch (opcion) {
		case 0:
			System.out.println("Adios, gracias por haber usado Youtube.");
			return false;
		case 1:
			nuevoCanal();
			return true;
		case 2:
			seleccionarCanal();
			return true;
		case 3:
			mostrarEstadisticas();
			return true;
		case 4:
			mostrarEstadisticasCompletas();
			return true;
		default:
			System.out.println("Opcion no valida. Intentelo de nuevo.");
			return true;
		}
	}

	private static int mostrarMenu() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("|----YOUTUBE-----|");
		System.out.println("1- Nuevo Canal");
		System.out.println("2- Seleccionar Canal");
		System.out.println("3- Mostrar estadisticas");
		System.out.println("4- Mostrar estadisticas completas");
		System.out.println("0- Salir");
		System.out.println("|----------------|");
		int option = scanner.nextInt();
		return option;
	}

	private static void nuevoCanal() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Introduce el nombre del canal: ");
		String nombre = scanner.nextLine();
		Canal canal = new Canal();
		canales.add(canal);
		canal.showMenu();
	}

	private static void seleccionarCanal() {
		Scanner scanner = new Scanner(System.in);

		if (canales.size() == 0) {
			nuevoCanal();
		} else {
			System.out.println("Selecciona Canal: ");
			Canal canal;
			for (int i = 0; i < canales.size(); i++) {
				canal = canales.get(i);
				System.out.println((i) + "-Nombre del canal:" + canal.getNombre() + " creando en fecha: "
						+ canal.getFechaDeCreacion() + " con " + canal.videos.size());
			}

			System.out.println("¿Que canal quieres?");
			int opcion = scanner.nextInt();
			Canal canalSeleccionado = canales.get(opcion);
			canalSeleccionado.showMenu();
		}
	}

	private static void mostrarEstadisticas() {

		System.out.println("Youtube tiene " + canales.size() + " canales.");
	}

	private static void mostrarEstadisticasCompletas() {
		Canal canal;
		Video video;
		Comentario comentario;
		System.out.println("Youtube tiene " + canales.size() + " canales.");
		for (int i = 0; i < canales.size(); i++) {
			canal = canales.get(i);
			System.out.println("- Nombre del canal: " + canal.getNombre() + " creado en fecha: "
					+ canal.getFechaDeCreacion() + " con " + canal.videos.size() + " videos");
			for (int j = 0; j < canal.videos.size(); j++) {
				video = Canal.videos.get(j);
				System.out.println("--Video: " + video.getNombre() + " en fecha: " + video.getFechaCreacion() + " con "
						+ video.getnumLike() + " y " + video.comentarios.size() + " comentarios.");
				for (int n = 0; n < Video.comentarios.size(); n++) {
					comentario = video.comentarios.get(n);
					System.out.println("---Comentario: " + comentario.getTexto() + " del usuario: "
							+ comentario.getNombre() + " en fecha: " + comentario.getFechaCreacion());

				}

			}

		}

	}

}
